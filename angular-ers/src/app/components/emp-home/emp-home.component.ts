import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IExpense } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-emp-home',
  templateUrl: './emp-home.component.html',
  styleUrls: ['./emp-home.component.css']
})
export class EmpHomeComponent implements OnInit {

  requests: IExpense[];

  constructor( private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getExpenses()
      .subscribe((expenses: IExpense[]) => this.requests = expenses);
  }

}
