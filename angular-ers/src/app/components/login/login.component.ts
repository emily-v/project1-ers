import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  loginSubmit() {
    console.log('oh hello');
    this.authService.authenticate(this.username, this.password, this.successful, this.failed);
    console.log(this.authService.authenticate);
  }

  successful() {
    console.log("login successful");
    return true;
  }
  failed() {
    console.log("login failed");
    return true;
  }

}
