import { Component, OnInit } from '@angular/core';
import { IExpense } from 'src/app/shared/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-exp',
  templateUrl: './new-exp.component.html',
  styleUrls: ['./new-exp.component.css']
})
export class NewExpComponent implements OnInit {

  expense: Expense;
  // todaysDate = this.getTodaysDate();


  constructor(private router: Router) { }

  ngOnInit() {
    this.expense = new Expense(1, "2019-01-17","2019-01-15",10,1,122.22,null,1, null)
  }

  submit() {
    if (this.expense) {
      console.log(this.expense);
      this.router.navigate(['/success-page']);
    } else {
      console.log("entry not valid"); // don't really need this bc submit disabled if not valid
    }
  }

  getTodaysDate() {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();
    return date;
  }


}
export class Expense {
  constructor(expId: number,
    submitDate: string,
    transDate: string,
    empId: number,
    expType: number,
    amount: number,
    merchant: string,
    status: number,
    resolveDate: string){}
}
