package org.base.connect;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.base.connect.Launcher;
import org.base.pojo.Reimb;
import org.base.pojo.User;

public class Launcher {
static SessionFactory sessionFactory;
	
	public static void main(String[] args) {
		new Launcher().insertInfo();
		Launcher launcher = new Launcher();

		for(User u : launcher.queryDemo("demo@gmail.com")) {
            System.out.println(u);
		}
	}
	
	private Properties setSessionFactoryProperties() {
		Properties temp = new Properties();
		 // information to create datasource
		temp.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
		temp.setProperty("hibernate.dialect.driver_class", "org.Postgresql.Driver");
		temp.setProperty("hibernate.connection.username", "username");
		temp.setProperty("hibernate.connection.password", "password");
		temp.setProperty("hibernate.connection.url", "jdbc:postgresql://sparknov27.cg0edppzdk1z.us-east-2.rds.amazonaws.com:5432/inclass");
		temp.setProperty("hibernate.connection.pool_size", "1");
		// behavior properties
		temp.setProperty("hibernate.hbm2ddl.auto", "create");
		temp.setProperty("hibernate.show_sql", "true");
		temp.setProperty("hibernate.format_sql", "true");

		return temp;
	}
	
	public void insertInfo() {
		// opening a new session and beginning transaction   
		Launcher launcher = new Launcher();
		sessionFactory = launcher.configureSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		    
		// creating inputs for database 
		User user = new User();
		user.setId(10);
		user.setEmail("demo@gmail.com"); 
		user.setFirstname("fname1");
		user.setLastname("lname1");
		user.setPassword("password");
		user.setRole(false);

		session.save(user);
		// testing
		System.out.println("Object save success");
		
		
		tx.commit();
		session.close();
	}

	
	 public List<User> queryDemo(String email) {
		 Session session = sessionFactory.openSession();
	     Transaction tx = session.beginTransaction();

	     List<User> u = null;

	     // HQL is OOP SQL
	     // HQL generate SQL queries based on Objects and their properties
	     String hql = "From User where email = :email";
	     Query q = session.createQuery(hql);
	     q.setString("email", email);

	     u = q.list();

	     tx.commit();
	     session.close();
	     return u;
	    }


	// connecting our session factory properties 
	public SessionFactory configureSessionFactory() {
		Properties props = new Properties();
		Configuration configuration = new Configuration().setProperties(setSessionFactoryProperties());
		// mappings
		configuration.addAnnotatedClass(User.class);
		configuration.addAnnotatedClass(Reimb.class);
		// registering configuration 
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
		return configuration.buildSessionFactory(serviceRegistry);
		
	}


}


