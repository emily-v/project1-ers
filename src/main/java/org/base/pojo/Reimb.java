package org.base.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="REIMB")
public class Reimb {
	
	@Id
	@Column(name="REIMB_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int reimbId;
	
	@Column(name="REIMB_TYPE")
	private String type;
	
	@Column(name="REIMB_STATUS")
	private boolean status;
	
	@Column(name="REIMB_AMOUNT")
	private double amount;
	
	@Column(name="REIMB_NOTES")
	private String description;
	
	
	public Reimb() {
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Reimb [type=" + type + ", status=" + status + ", amount=" + amount + ", description=" + description
				+ "]";
	}

}

